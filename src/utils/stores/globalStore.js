import React from "react";
import axios from "axios";
import api_link from "../configs/api_links";

const ControlsStateContext = React.createContext();
const ControlsDispatchContext = React.createContext();

function ControlsReducer(state, action) {
  switch (action.type) {
    case "init": {
      return { ...state, error: undefined, loading: true };
    }
    case "init_settings": {
      return { ...state, error: undefined, loading_settings: true };
    }
    case "init_update": {
      return { ...state, loading_update: true };
    }

    case "error": {
      return {
        ...state,
        fetch_all_folders_request_loading: false,
        loading: false,
        loading_settings: false,
        error: action.payload,
        loading_update: false,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function GlobalProvider({ children }) {
  const [state, dispatch] = React.useReducer(ControlsReducer, {
    loading: true,
    error: undefined,
  });
  return (
    <ControlsStateContext.Provider value={state}>
      <ControlsDispatchContext.Provider value={dispatch}>{children}</ControlsDispatchContext.Provider>
    </ControlsStateContext.Provider>
  );
}

function useStoreState() {
  const context = React.useContext(ControlsStateContext);
  if (context === undefined) {
    throw new Error("useStoreState must be used within a GlobalProvider");
  }
  return context;
}

function useStoreDispatch() {
  const context = React.useContext(ControlsDispatchContext);
  if (context === undefined) {
    throw new Error("useStoreDispatch must be used within a GlobalProvider");
  }
  return context;
}

export { GlobalProvider, useStoreState, useStoreDispatch };
