import styled from "styled-components";
import theme from "../theme";

const EmptySpace = styled.div`
  height: ${(props) => {
    return props.height ? props.height + "px" : theme.sizes.header.height + "px";
  }};
`;

export default EmptySpace;
