const getReadableFileSizeString = (fileSizeInBytes) => {
  let i = -1;
  let byteUnits = [" kB", " MB", " GB", " TB", "PB", "EB", "ZB", "YB"];
  do {
    fileSizeInBytes = fileSizeInBytes / 1024;
    i++;
  } while (fileSizeInBytes > 1024);

  return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
};

const testUnitaire = (confidences = [0.97, 0.93, 0.62, 0.52, 0.46, 0.33, 0.12], detections = [1, 0, 1, 1, 0, 0, 0]) => {
  let s = [];
  for (let i = 0; i < confidences.length; i++) {
    let seuil = confidences[i];
    let fp = 0,
      vp = 0,
      fn = 0;
    for (let j = 0; j < confidences.length; j++) {
      if (detections[j] === 1 && confidences[j] >= seuil) {
        // VP
        vp++;
      } else if (detections[j] === 0 && confidences[j] >= seuil) {
        // FP
        fp++;
      } else if (detections[j] === 1 && confidences[j] < seuil) {
        // FN
        fn++;
      }
    }
    let precision = vp / (vp + fp);
    let recall = vp / (vp + fn);
    let fscore = (2 * (precision * recall)) / (precision + recall);
    s.push({ fscore: fscore, precision: precision, recall: recall });
  }
};

const  get_obj_val = (obj, val) =>  {
  if (val.split(".").length <= 1) {
    return Array.isArray(obj[val]) ? JSON.stringify(obj[val]) : obj[val];
  } else {
    return get_obj_val(obj[val.split(".")[0]], val.split(".").slice(1).join("."));
  }
}


const filter_by_attr = ({ query, obj, attrs }) => {
  if (!query) return true;
  for (let attr of attrs) {
    if (get_obj_val(obj, attr).toLowerCase().includes(query.toLowerCase())) {
      return true;
    }
  }
  return false;
}

export { getReadableFileSizeString, filter_by_attr, get_obj_val };
