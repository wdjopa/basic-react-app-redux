import React from "react";
import { Router } from "react-router-dom";
import { createBrowserHistory } from "history";
import "antd/dist/antd.css";
import styled from "styled-components";
import GlobalStyle from "./utils/styles/globalStyles";
import { version, title } from "../package.json";
import { GlobalProvider } from "./utils/stores/globalStore";
import theme from "./utils/styles/theme";
import RoutedContainer from "./RoutedContainer";

const history = createBrowserHistory();


const App = (props) => {
  return (
    <Router history={history}>
        <GlobalProvider>
          <RoutedContainer />
        </GlobalProvider>
        <GlobalStyle />
    </Router>
  );
};

export default App;
