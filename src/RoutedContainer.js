import React, { Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import "antd/dist/antd.css";

const RoutedContainer = (props) => {
  return (
    <Suspense fallback>
      <Switch>
        <Route exact path="/" />
      </Switch>
    </Suspense>
  );
};

export default RoutedContainer;
